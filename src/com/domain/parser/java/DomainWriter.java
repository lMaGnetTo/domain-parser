package com.domain.parser.java;

import java.io.FileWriter;
import java.io.IOException;

/** Класс служит для записи информации о доменных именах в указаный файл.
 * @autor Asanov Arsen
 * @version 1.0
 */
class DomainWriter {

    /** Список заголовков для выходного файла*/
    private final String[] headlines = {"Domain Name", "Creation Date", "Updated Date", "Expiration Date"};

    /** Разделитель, используемый в csv-файле*/
    private char divider;

    /** Объект для записи*/
    private FileWriter writer;

    /** Количество доменных имен, которые еще предстоит проверить*/
    private int domains;

    /** Создает новый объект класса с заданными значениями и вписывает в выходной файл заголовки - {@link DomainWriter#headlines}
     * @param divider - разделитель
     * @param domains - количество доменных имен в исходном файле
     * @param outFilePath - путь к выходному файлу
     */
    DomainWriter(char divider, int domains, String outFilePath) {
        this.domains = domains;
        this.divider = divider;
        try {
            writer = new FileWriter(outFilePath);
            for (int i = 0; i < headlines.length; i++) {
                if (i < headlines.length - 1)
                    writer.write(headlines[i] + divider);
                else writer.write(headlines[i]);
            }
            writer.write('\n');
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /** Получает информацию об одном доменном имени и записывает её в выходной файл.
     * После записи информации о последнем доменном имени закрывает поток записи.
     * @param domainInfo - информация об одном доменном имени
     */
    public synchronized void write(String[] domainInfo) {
        try {
            for (int i = 0; i < domainInfo.length; i++) {
                if (i < domainInfo.length - 1)
                    writer.write(domainInfo[i] + divider);
                else {
                    writer.write(String.valueOf(domainInfo[i]));
                }
            }
            writer.write('\n');
        } catch (IOException e) {
            e.printStackTrace();
        }

        domains -= 1;
        if (domains == 0) {
            try {
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
