package com.domain.parser.java;


import java.util.LinkedList;
import java.util.Scanner;

/**
 * Класс функции "main"
 *
 * @version 1.0
 */
public class Main {

    /**
     * При запуске программы выводит на консоль "меню".
     */
    public static void main(String[] args) {
        String filePath;
        String outFilePath;
        int index = 0;
        Scanner scanner = new Scanner(System.in);

        /** Цикл "while" предназначен для продолжения работы с программой после выполнения одной из поераций.*/
        while (index != 3) {
            System.out.println("1 -> Simple check\n2 -> Quick check\n3-> Exit the program\n\t Select the action... ");
            try {
                index = Integer.parseInt(scanner.nextLine());
            } catch (NumberFormatException e) {
                System.err.println("Please, enter a valid menu number");
                continue;
            }

            switch (String.valueOf(index)) {
                /** "case 1" -> Simple check\n
                 * "case 2" -> Quick check\n
                 * "case 3"-> Exit the program*/
                case "1":
                    System.out.println("Enter path to file with your domains:");
                    filePath = scanner.nextLine();
                    System.out.println("Enter path to output file(Default - working directory):");
                    outFilePath = scanner.nextLine();
                    if (outFilePath.equals(""))
                        outFilePath = "DomainInfo.csv";
                    LinkedList<String> domains = DomainReader.read(filePath);
                    DomainWriter domainWriter = new DomainWriter(',', domains.size(), outFilePath);

                    ThreadManager threadManager = new ThreadManager(domainWriter);
                    threadManager.devide(domains);
                    break;
                case "2":
                    System.out.print("Enter domain name: ");
                    String[] arr = Parser.parse(scanner.nextLine());
                    System.out.println("======================================");
                    System.out.println("Domain name:\t" + arr[0]);
                    System.out.println("Creation date:\t" + arr[1]);
                    System.out.println("Updated date:\t" + arr[2]);
                    System.out.println("Expiration date:\t" + arr[3]);
                    System.out.println("======================================" + '\n');
                    break;
                case "3":
                    System.out.println("Goodbye!");
                    break;
                default:
                    System.out.println("Sorry, I didn`t understand you...");
                    break;
            }
        }
    }
}
