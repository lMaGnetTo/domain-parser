package com.domain.parser.java;

import java.util.LinkedList;
/** Данный класс описывает структуру поведения потоков
 * @author Minzatov Nazim
 * @version 1.0
 */
class Threader implements Runnable {
    /** От какого доменного имени начинается счёт*/
    private int prev;
    /** До какого доменного имени*/
    private int next;
    /** Список доменных имён*/
    private LinkedList<String> domains;
    /** Производит запись в файл */
    private DomainWriter domainWriter;

    public Threader(int prev, int next, LinkedList<String> domains, DomainWriter domainWriter) {
        this.prev = prev;
        this.next = next;
        this.domains = domains;
        this.domainWriter = domainWriter;
    }

    /** То, что исполняет каждый поток: парсит свои доменные имена и записывает их в domainWriter */
    public void run() {
        for (int i = prev; i < next; i++) {
            domainWriter.write(Parser.parse(domains.get(i)));
        }
    }
}