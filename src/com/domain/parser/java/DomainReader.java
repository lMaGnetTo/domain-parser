package com.domain.parser.java;

import java.io.FileInputStream;
import java.util.LinkedList;
import java.util.Scanner;

/** Класс служит для считывания списка доменов
 * с файла.
 * @autor Asanov Arsen
 * @version 1.0
 */
public class DomainReader {

    /** Список для доменных имен */
    private static LinkedList<String> domains = new LinkedList<>();

    /** Считывает доменные имена с файла и записывает их список доменов {@link DomainReader#domains}
     * @return Возвращает список доменных имён
     * @param filePath - путь к исходному файлу с доменными именами
     */
    public static LinkedList<String> read(String filePath) {
        try {
            Scanner scanner = new Scanner(new FileInputStream(filePath));
            while (scanner.hasNext()) {
                domains.add(scanner.nextLine());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return domains;

    }
}
