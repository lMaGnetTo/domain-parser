package com.domain.parser.java;

import java.util.LinkedList;
/** Класс служит для создания потоков и определения их количества
 * в зависимости от количества доменных имён
 * @author Minzatov Nazim
 * @version 1.0
 */
class ThreadManager {
    private DomainWriter writer;

    ThreadManager(DomainWriter writer) {
        this.writer = writer;
    }
    /** метод для создания потоков
     * @param domains - доменные имена
     */
    public void devide(LinkedList<String> domains) {

        /** Переменная отвечающая за количество потоков*/
        int num_threads;

        /* В зависимости от количества доменных имён указываем нужное нам количество потоков*/
        switch (domains.size()/10){
            case 0:
                num_threads = 1;
                break;
            case 1:
                num_threads = 2;
                break;
            case 2:
                num_threads = 2;
                break;
            case 3:
                num_threads = 4;
                break;
            case 4:
                num_threads = 5;
                break;
            case 5:
                num_threads = 7;
                break;
            case 6:
                num_threads = 9;
                break;
            default:
                num_threads = 10;
                break;
        }
        /* У каждого потока есть определённое число доменных имён.
         * У всех потоков это число одинаково, кроме последнего,
         * так как кроме стандартного количества потоков
         * к нему относится и "остаток" потоков */

        /** Количество потоков
         */
        int domainPerThread = domains.size() / num_threads;

        /** "остаток" потоков */
        int domainsRemain = domains.size() % num_threads;

        /** От какого доменного имени начинается счёт */
        int threadStart = 0;

        /** До какого доменного имени */
        int threadEnd = 0;

        /* Создаём потоки, передаём им аргументы и запускаем их */
        Threader r;
        for (int i = 0; i < num_threads - 1; i++) {
            /** Указывает параметры для потока
             * @param threadStart - От какого доменного имени начинается счёт
             * @param domainPerThread + threadEnd - До какого доменного имени
             * @param domains - доменные имена
             * @param writer - файл для записи
             */
            r = new Threader(threadStart, domainPerThread + threadEnd, domains, writer);
            Thread f = new Thread(r);
            f.start();
            threadStart = threadEnd + domainPerThread;
            threadEnd = threadEnd + domainPerThread;
        }

        /* Создаём последний поток */
        /** Указывает параметры для потока
         * @param threadStart - От какого доменного имени начинается счёт
         * @param domainPerThread + threadEnd - До какого доменного имени
         * @param domains - доменные имена
         * @param writer - файл для записи
         */
        r = new Threader(threadStart, domainPerThread + threadEnd + domainsRemain, domains, writer);
        new Thread(r).start();

    }
}