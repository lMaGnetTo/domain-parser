/**
 *  Класс используется для парсинга URL GET запроса к сервису www.domaininformation.de по селектору "pre".
 *  @author magnetto
 *  @version 1.0 stable
 */

package com.domain.parser.java;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.io.IOException;
import java.util.Scanner;


class Parser {
    /** Метод, возвращающий массив с информацией о доступности входного домена.
     * @param url - ссылка на домен для проверки доступности */
    public static String[] parse(String url) {
        /** Объект, хранящий ответ от сервера на запрос в виде html */
        Document doc = null;
        /** Объект для итерации по html документу */
        Scanner sc;
        /** Строка для обработки каждого элемента массива поотдельности */
        String line;
        /** Результирующий массив с информацией о домене */
        String[] result = new String[4];
        /** Временный массив для обработки доменных имён */
        String[] tmp;

        /**
         * GET запрос к сервису для проверки входного доменного имени
         * В случае исключения кладёт в массив соответсвующие строки о невозможности обработки
         * входного домена
         */
        try {
            doc = Jsoup.connect("http://www.domaininformation.de/whois/" + url).get();
        } catch (IOException e) {
            result[0] = url;
            result[1] = "unable to check";
            result[2] = "unable to check";
            result[3] = "unable to check";
            return result;
        }

        /** Парсинг html документа по селектору "pre", который хранит
         * всю информацию о проверенном хостинге.
         * В случае, если домен не занят, записывает в массив соответсвующую строку.
         */
        sc = new Scanner(doc.select("pre").text());

        if (doc.select("pre").text().contains("No match for")) {
            result[0] = url;
            result[1] = "domain is free";
            result[2] = "domain is free";
            result[3] = "domain is free";
            return result;
        }

        /** В случае, если искомый домен занят, в массив записываются поля:
         * Domain name, creation data, updated data и Expiry data для заданного домена. */
        while (sc.hasNextLine()) {
            line = sc.nextLine();

            tmp = url.split("/");
            result[0] =  tmp[tmp.length-1];

            if (line.split(":")[0].contains("Creation Date") || line.split(":")[0].contains("created")) {
                result[1] = line.split(":")[1];
            }

            if (line.split(":")[0].contains("Updated Date") || line.split(":")[0].contains("paid-till")) {
                result[2] = line.split(":")[1];
            }


            if (line.split(":")[0].contains("Expiry Date") || line.split(":")[0].contains("free-date") || line.split(":")[0].contains("Expiration Date")) {
                result[3] = line.split(":")[1];
            }
        }

        return result;
    }
}